package main

import "fmt"

var numero int
var texto string
var status bool = true

func main() {
	numero2, numero3, numero4, texto, status := 2, 5, 67, "Este es mi texto", false

	var moneda int64 = 0
	numero2 = int(moneda)

	texto = fmt.Sprintf("%d", moneda)

	fmt.Println(numero2)
	fmt.Println(numero3)
	fmt.Println(numero4)
	fmt.Println(texto)
	fmt.Println(status)

	viewStatus()
}

func viewStatus() {
	fmt.Println(status)
}
