package main

import "fmt"

func main() {
	exponenencia(2)
}

func exponenencia(numero int) {
	if numero > 100000000 {
		return
	}
	fmt.Println(numero)
	exponenencia(numero * 2)
}
