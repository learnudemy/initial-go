package main

import "fmt"

func main() {

	//While
	i := 1
	for i < 10 {
		fmt.Println(i)
		i++
	}

	for j := 0; j < 10; j++ {
		fmt.Println(j)
	}

	numero := 0
	for {
		fmt.Println("Continuo")
		fmt.Println("Ingrese el número secreto")
		fmt.Scanln(&numero)

		if numero == 100 {
			fmt.Println("Has ingresado el Número Secreto Felicitaciones!")
			break
		}
	}

	var x = 0
	for x < 10 {
		fmt.Printf("\n Valor de x: %d", x)

		if x == 5 {
			fmt.Printf(" multiplicamos por 2 \n")
			x = x * 2
			continue
		}

		fmt.Printf("             Pasó por aquí \n")
		x++
	}

	var y int = 0
RUTINA:
	for y < 10 {
		if y == 4 {
			y = y + 2
			fmt.Println("voy a rutina sumando 2 a y")
			goto RUTINA
		}

		fmt.Printf("Valor de y: %d\n", y)
		y++
	}
}
