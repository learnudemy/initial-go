package main

import "fmt"

var tabla [10]int
var matriz [5][7]int

func main() {
	tabla[0] = 1
	tabla[5] = 15

	fmt.Println(tabla)

	uno()
	dos()
	tres()
}

func uno() {
	tabla2 := [10]int{5, 6, 98, 1, 2, 43, 32, 45, 86, 99}

	for i := 0; i < len(tabla2); i++ {
		fmt.Println(tabla2[i])
	}

}

func dos() {
	matriz[3][5] = 1
	fmt.Println(matriz)

}

func tres() {
	//Slice
	tabla3 := []int{2, 5, 4}
	fmt.Println(tabla3)

	variante2()
	variante3()
	variante4()
}

func variante2() {
	elementos := [5]int{2, 5, 4, 7, 9}
	//Slice 3
	porcion := elementos[3:]

	fmt.Println(porcion)
}

func variante3() {
	elementos := make([]int, 5, 20)

	fmt.Printf("Largo %d, Capacidad %d \n", len(elementos), cap(elementos))
}

func variante4() {
	nums := make([]int, 0, 0)

	for i := 0; i < 100; i++ {
		nums = append(nums, i)
	}

	fmt.Printf("Largo %d, Capacidad %d \n", len(nums), cap(nums))

}
